﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace TestHelloWorld.Controllers
{
    public class HomeController : AsyncController
    {
        // GET: Home
        public async Task<ActionResult> Index()
        {
            ViewBag.Test = "Hello";
            ViewBag.Msg = "World";
            return View();
        }

        
    }
}